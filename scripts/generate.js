import { loadDatabase } from '../src/providers/Database';
import User from '../src/models/User';
import Treasury from '../src/models/Treasury';
import { usersDetails, vendorsDetails, clubDetails } from '../src/providers/Constants';
import Log from '../src/utils/logger';

const createOrUpdateTreasury = async (treasury, index) => {
  const inputObject = {
    user: treasury._id || undefined,
    holding_amount: 0,
    spent_amount: 0,
    investment: 0,
    investment_return: 0,
    received: 0,
    number_of_paid_months: 0,
    value: treasury.value,
  };
  let treasuryData = await Treasury.findOne({ value: treasury.value });
  if (!treasuryData) {
    treasuryData = await Treasury.create(inputObject);
    Log.info(`(index: ${(`0${index + 1}`).slice(-2)} ) :: Create treasury : ${treasury.value}.`);
  }
  Log.info(`(index: ${(`0${index + 1}`).slice(-2)} ) :: Found treasury : ${treasury.value}.`);
  return treasuryData;
};

const createOrUpdateUser = async (user, index) => {
  let userData = await User.findOneAndUpdate(
    { value: user.value }, user,
  );
  if (!userData) {
    userData = await User.create(user);
    Log.info(`(index: ${(`0${index + 1}`).slice(-2)} ) :: Created user : ${user.value}.`);
    return userData;
  }

  Log.info(`(index: ${(`0${index + 1}`).slice(-2)} ) :: Updated user : ${user.value}.`);
  return userData;
};

loadDatabase().then(async () => {
  const users = [];
  try {
    const mapper = [...usersDetails, ...vendorsDetails].map(async (each, index) => {
      const userData = await createOrUpdateUser(each, index);
      users.push(userData);
    });
    await Promise.allSettled(mapper);
    const userMapper = users.map(async (each, index) => {
      await createOrUpdateTreasury(each, index);
    });
    await Promise.allSettled(userMapper);
    await createOrUpdateTreasury(clubDetails, 0);
    process.exit(0);
  } catch (_err) {
    Log.error('templateGenerate :: ', _err);
    process.exit(1);
  }
});
