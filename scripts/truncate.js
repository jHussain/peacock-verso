import { loadDatabase } from '../src/providers/Database';
import User from '../src/models/User';
import Transactions from '../src/models/Transactions';
import Treasury from '../src/models/Treasury';
import Log from '../src/utils/logger';

loadDatabase().then(async () => {
  try {
    Log.info('Truncate :: Truncate stated!');
    Log.info('Truncate :: Users deleting!');
    await User.deleteMany({});
    Log.info('Truncate :: Users deleted!');
    Log.info('Truncate :: Transactions deleting!');
    await Transactions.deleteMany({});
    Log.info('Truncate :: Transactions deleted!');
    Log.info('Truncate :: Treasury deleting!');
    await Treasury.deleteMany({});
    Log.info('Truncate :: Treasury deleted!');
    process.exit(0);
  } catch (_err) {
    Log.error('templateGenerate :: ', _err);
    process.exit(1);
  }
});
