import Log from './logger';
import { errorRespond, SHORT_MSG } from './responder';

export const clusterEventsHandler = (_cluster) => {
  // Catch cluster listening event...
  _cluster.on('listening', (worker) => Log.info(`CLUSTER :: Cluster with ProcessID '${worker.process.pid}' Connected!`));

  // Catch cluster once it is back online event...
  _cluster.on('online', (worker) => Log.info(`CLUSTER :: Cluster with ProcessID '${worker.process.pid}' has responded after it was forked! `));

  // Catch cluster disconnect event...
  _cluster.on('disconnect', (worker) => Log.info(`CLUSTER :: Cluster with ProcessID '${worker.process.pid}' Disconnected!`));

  // Catch cluster exit event...
  _cluster.on('exit', (worker, code, signal) => {
    Log.info(`CLUSTER :: Cluster with ProcessID '${worker.process.pid}' is Dead with Code '${code}, and signal: '${signal}'`);
    // Ensuring a new cluster will start if an old one dies
    _cluster.fork();
  });
};

export const processEventsHandler = () => {
  // Catch the Process's uncaught-exception
  process.on('uncaughtException', (exception) => Log.error(exception));

  // Catch the Process's warning event
  process.on('warning', (warning) => Log.warn(warning));
};

// eslint-disable-next-line no-unused-vars
export const errorHandler = (err, req, res, next) => {
  Log.error(err);
  return errorRespond(res, SHORT_MSG.unexpected_server_error, 500);
};

export const notFoundHandler = (_express) => {
  _express.all('*', (req, res) => {
    // ip from client header or from express request object
    const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    Log.error(`Path '${req.originalUrl}' not found [IP: '${ip}']!`);
    return errorRespond(res, SHORT_MSG.url_not_found, 404);
  });
  return _express;
};
