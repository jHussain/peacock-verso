import Log from './logger';
import { getCatch, setCatch } from '../service/catcher';

const catcherClearURL = ['/api/treasury/cleanCatch', '/api/action/backup', '/api/action/restore'];

export const successRespond = async (req, res, msg, code, data, payload = {}, isCached = false) => {
  if (!catcherClearURL.includes(req.originalUrl) && req.method === 'GET' && !isCached) {
    await setCatch(req.originalUrl, { msg, code, data, payload });
  }
  return res
    .status(code)
    .json({ success: true, code, msg, data, ...payload })
    .end();
};

export const errorRespond = (res, msg, code, data = {}) => res
  .status(200)
  .json({ success: false, code, msg, data })
  .end();

export const SHORT_MSG = {
  successful: 'successful',
  unsuccessful: 'unsuccessful',
  invalid_password: 'invalid_password',
  self_transfer: 'self_transfer',
  from_insufficient_balance: 'from_insufficient_balance',
  to_insufficient_balance: 'to_insufficient_balance',
  url_not_found: 'url_not_found',
  unexpected_error: 'unexpected_error',
  unexpected_server_error: 'unexpected_server_error',
  no_authorization: 'no_authorization',
  has_authorization: 'has_authorization',
  invalid_authorization: 'invalid_authorization',
  unexpected_authorization_error: 'unexpected_authorization_error',
  no_app_secret: 'no_app_secret',
  invalid_app_secret: 'invalid_app_secret',
  unexpected_app_secret_error: 'unexpected_app_secret_error',
  notify_success: 'notify_success',
  notify_failed: 'notify_failed',
};

export const handleRedisCatch = async (req, res, next) => {
  Log.info(`REDIS :: Fetching cached data ${req.originalUrl}`);

  if (req.method !== 'GET' || req.originalUrl === catcherClearURL) {
    return next();
  }
  const { isCached, cachedData } = await getCatch(req.originalUrl);
  if (!isCached) {
    return next();
  }
  Log.info('REDIS :: Fetching cached data');
  const { msg, code, data, payload } = cachedData;
  return successRespond(req, res, msg, code, data, payload, true);
};
