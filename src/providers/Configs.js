import dotenv from "dotenv";
import Log from "../utils/logger";

Log.info("ENV :: Registering the env file");
dotenv.config();

Log.info("ENV :: Initializes the configs");
export const ENV = process.env.NODE_ENV || "development";
export const PORT = process.env.PORT || 5000;
export const ENABLE_CHILD_PROCESS = process.env.ENABLE_CHILD_PROCESS === "YES";
export const MONGO_DB_URL = process.env.MONGO_DB_URL || "";
export const IS_PRODUCTION = ENV === "production";
export const APP_NAME = process.env.APP_NAME || "Peacock";
export const APP_EDIT_PASSWORD =
  process.env.APP_EDIT_PASSWORD || "peacock_club";

export const ENABLE_REDIS = Boolean(process.env.ENABLE_REDIS === "YES");
export const REDIS_HOST = process.env.REDIS_HOST || "localhost";
export const REDIS_PORT = process.env.REDIS_PORT || "6379";
export const REDIS_PASSWORD = process.env.REDIS_PASSWORD || "";

export const DISCORD_BOT_ID =
  process.env.DISCORD_BOT_ID || "964139950088007761";
export const DISCORD_BOT_TOKEN =
  process.env.DISCORD_BOT_TOKEN ||
  "OTY0MTM5OTUwMDg4MDA3NzYx.YlgTTA.r5wr5bj0N-XFWbahu1XgL82N-Uk";
export const DISCORD_GUILD = process.env.DISCORD_BOT_ID || false;
export const USE_DISCORD_USERS_IN_OPTIONS =
  process.env.USE_DISCORD_USERS_IN_OPTIONS === "YES" || false;
export const DB_BACKUP_PATH = process.env.DB_BACKUP_PATH || "";

export default {
  PORT,
  ENV,
  ENABLE_CHILD_PROCESS,
  IS_PRODUCTION,
  MONGO_DB_URL,
  APP_NAME,
  APP_EDIT_PASSWORD,
  REDIS_HOST,
  REDIS_PORT,
  REDIS_PASSWORD,
  DISCORD_BOT_ID,
  DISCORD_BOT_TOKEN,
  DISCORD_GUILD,
  USE_DISCORD_USERS_IN_OPTIONS,
  DB_BACKUP_PATH,
};
