import * as redis from 'redis';
import Log from '../utils/logger';
import { ENABLE_REDIS, REDIS_HOST, REDIS_PORT, REDIS_PASSWORD } from './Configs';

const redisClient = redis.createClient({
  host: REDIS_HOST,
  port: REDIS_PORT,
  password: REDIS_PASSWORD,
});

redisClient.on('error', (_err) => Log.error('REDIS :: connectToRedis/error :: ', _err));

redisClient.on('ready', () => Log.info('REDIS :: Redis server is ready to accept commands'));

export const connectToRedis = async () => {
  Log.debug(`REDIS :: Initializes the redis client ${REDIS_HOST}:${REDIS_PORT}||password:${REDIS_PASSWORD}`);
  if (ENABLE_REDIS) {
    Log.debug(`REDIS :: Connecting to redis server at: ${REDIS_HOST}:${REDIS_PORT}`);
    await redisClient.connect();
  }
};

export default redisClient;
