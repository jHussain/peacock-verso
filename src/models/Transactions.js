import mongoose from '../providers/Database';

// Define the Transaction Schema
export const transactionSchema = new mongoose.Schema(
  {
    transactionOn: { type: Date, default: new Date() },
    transactionType: {
      type: String,
      enums: ['debit', 'credit'],
    },
    amount: { type: Number, default: 0 },
    method: {
      type: String,
      enums: ['deposit', 'withdraw', 'transfer', 'expenditure', 'invest', 'return_on_invest'],
    },
    from: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    to: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    notes: { type: String, default: '' },
    deleted: { type: Boolean, default: false },
  },
  { timestamps: true },
);

export default mongoose.model('Transaction', transactionSchema, 'transactions');
