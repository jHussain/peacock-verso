import Transactions from '../models/Transactions';
import Log from '../utils/logger';
import { getFromTo } from './user';
import { COMMANDS_KEY } from '../utils/commands';
import { generateTreasuryList } from './treasury';
import { SHORT_MSG } from '../utils/responder';

export const allTransactions = async (where = {}, skip, limit, sortBy = 'transactionOn') => {
  try {
    const transactionList = await Transactions.find({ deleted: false, ...where })
      .populate({
        path: 'from',
        select: 'name value image role',
      })
      .populate({
        path: 'to',
        select: 'name value image role',
      })
      .skip(skip)
      .limit(limit)
      .lean()
      .sort({ [sortBy]: -1 })
      .exec();

    return {
      success: true,
      data: transactionList,
    };
  } catch (_err) {
    Log.error('allTransaction :: ', _err);
    return {
      success: false,
      data: [],
    };
  }
};

const handleTransaction = (commandName) => {
  const data = {
    transactionType: 'credit',
    method: commandName,
  };
  // eslint-disable-next-line max-len
  if (
    [
      COMMANDS_KEY.withdraw,
      COMMANDS_KEY.expenditure,
      COMMANDS_KEY.invest,
    ].includes(commandName)
  ) {
    data.transactionType = 'debit';
  }
  return data;
};

export const addTransaction = async (commandName, amount, from, to, transactionOn, notes) => {
  Log.debug('addTransaction :: input :: %o', {
    commandName,
    amount,
    from,
    to,
    transactionOn,
    notes,
  });
  const { transactionType, method } = handleTransaction(commandName);
  Log.debug('addTransaction :: handleTransaction :: %o', {
    amount,
    transactionType,
    method,
  });

  if (COMMANDS_KEY.transfer === commandName && from === to) {
    return {
      success: false,
      data: {},
      msg: SHORT_MSG.self_transfer,
    };
  }

  const { success, data } = await getFromTo(from, to);

  if (!success) {
    return {
      success: false,
      data: {},
    };
  }

  const { fromUser, toUser } = data;
  // eslint-disable-next-line max-len
  if (
    [
      COMMANDS_KEY.withdraw,
      COMMANDS_KEY.expenditure,
      COMMANDS_KEY.invest,
      COMMANDS_KEY.transfer,
    ].includes(commandName)
  ) {
    const fromHolding = Number(fromUser.treasury.holding_amount) || 0;
    if (fromHolding - (Number(amount) || 0) < 0) {
      return {
        success: false,
        data: {},
        msg: SHORT_MSG.from_insufficient_balance,
      };
    }
    if (COMMANDS_KEY.withdraw === commandName && toUser) {
      const toInvestment = Number(toUser.treasury.investment) || 0;
      if (toInvestment - (Number(amount) || 0) < 0) {
        return {
          success: false,
          data: {},
          msg: SHORT_MSG.to_insufficient_balance,
        };
      }
    }
  }
  try {
    const createTransactions = new Transactions({
      transactionOn: new Date(transactionOn) || new Date(),
      transactionType,
      amount,
      method,
      from: fromUser,
      to: toUser,
      notes,
      deleted: false,
    });
    const created = await createTransactions.save();
    if (!created._id) {
      return {
        success: false,
        data: {},
      };
    }
    if (created?._id) {
      await generateTreasuryList();
    }
    return {
      success: true,
      data: created,
    };
  } catch (_err) {
    Log.error('allTransaction :: ', _err);
    return {
      success: false,
      data: {},
    };
  }
};

export const deleteTransaction = async (transactionId) => {
  try {
    const transaction = await Transactions.findOneAndUpdate(
      { _id: transactionId },
      {
        deleted: true,
      },
      {
        new: true,
      },
    );

    if (transaction?.delete) {
      await generateTreasuryList();
    }
    return {
      success: transaction?.deleted || false,
      data: transaction,
    };
  } catch (_err) {
    Log.error('deleteTransaction :: ', _err);
    return {
      success: false,
      data: {},
    };
  }
};
