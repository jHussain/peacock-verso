import Log from '../utils/logger';
import { successRespond, errorRespond, SHORT_MSG } from '../utils/responder';
import {
  allTransactions,
  addTransaction,
  deleteTransaction,
} from '../service/transaction';
import { transformToTable } from '../utils/transform';
import { APP_EDIT_PASSWORD } from '../providers/Configs';

export const transactionListController = async (req, res) => {
  Log.debug('transactionListController :: %o', {
    body: req.body,
    query: req.query,
  });
  const { method, eMethod, user, role, sortBy } = req.query;
  const limit = Number(req.query.limit) || 10;
  const page = Number(req.query.page) || 1;
  const skip = (page - 1) * limit || 0;

  const where = {};
  if (method) {
    where.method = method;
  }
  if (user) {
    where[role] = user;
  }
  if (eMethod) {
    where.method = {
      $ne: eMethod,
    };
  }

  Log.debug('transactionListController :: updated :: %o', {
    where,
    limit,
    page,
    skip,
  });

  const { data, success } = await allTransactions(where, skip, limit, sortBy);
  if (!success) {
    return errorRespond(res, SHORT_MSG.unexpected_error, 500);
  }
  return successRespond(req, res, SHORT_MSG.successful, 202, {
    content: transformToTable(data),
    method,
    user,
    role,
    limit,
    page,
    skip,
  });
};

export const deleteTransactionController = async (req, res) => {
  Log.debug('deleteTransactionController :: %o', {
    body: req.body,
    query: req.query,
  });
  const { transactionId, password } = req.body;

  if (!transactionId) {
    return errorRespond(res, SHORT_MSG.unsuccessful, 500);
  }

  if (!password || APP_EDIT_PASSWORD !== password) {
    return errorRespond(res, SHORT_MSG.invalid_password, 500);
  }
  const { success } = await deleteTransaction(transactionId);

  if (!success) {
    return errorRespond(res, SHORT_MSG.unsuccessful, 500);
  }
  return successRespond(req, res, SHORT_MSG.successful, 202, {});
};

export const addTransactionController = async (req, res) => {
  Log.debug('addTransactionController :: req %o', {
    body: req.body,
    query: req.query,
  });

  const { amount, from, to, method, notes, password, transactionOn } = req.body;

  if (!amount || !from || !to || !method) {
    return errorRespond(res, SHORT_MSG.unsuccessful, 500);
  }

  if (!password || APP_EDIT_PASSWORD !== password) {
    return errorRespond(res, SHORT_MSG.invalid_password, 500);
  }
  Log.debug('addTransactionController :: calling/addTransaction');
  const { success, data, msg } = await addTransaction(
    method,
    amount,
    from,
    to,
    transactionOn,
    notes,
  );

  Log.debug('addTransactionController :: addTransaction :: %o', { success, data, msg });

  if (!success) {
    return errorRespond(res, msg || SHORT_MSG.unsuccessful, 500);
  }
  return successRespond(req, res, SHORT_MSG.successful, 202, data);
};

export const name = 'Transaction';
