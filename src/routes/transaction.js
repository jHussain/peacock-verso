import { Router } from 'express';
import { transactionListController, deleteTransactionController, addTransactionController } from '../controllers/transactionController';

const router = Router();

router.post('/', addTransactionController);
router.get('/', transactionListController);
router.post('/delete', deleteTransactionController);

export default router;
