import Log from "../utils/logger";
import userRouter from "./user";
import treasuryRouter from "./treasury";
import transactionRouter from "./transaction";
import actionRouter from "./action";
import { handleRedisCatch } from "../utils/responder";

export default (app) => {
  Log.info("Routes :: Mounting API Routes...");

  Log.info("REDIS :: Initializes redis middleware...");
  app.all("*", handleRedisCatch);

  app.use("/api/user", userRouter);
  app.use("/api/transaction", transactionRouter);
  app.use("/api/treasury", treasuryRouter);
  app.use("/api/action", actionRouter);
  return app;
};
